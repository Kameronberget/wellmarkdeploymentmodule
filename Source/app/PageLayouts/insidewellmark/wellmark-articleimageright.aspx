﻿<%@ Page language="C#"   Inherits="Microsoft.SharePoint.Publishing.PublishingLayoutPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePointWebControls" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingWebControls" Namespace="Microsoft.SharePoint.Publishing.WebControls" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="PublishingNavigation" Namespace="Microsoft.SharePoint.Publishing.Navigation" Assembly="Microsoft.SharePoint.Publishing, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceholderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            getComments();
            likepage.checkUserLikedPage();
            
        });
    </script>
	<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
	<PublishingWebControls:EditModePanel runat="server">
		<!-- Styles for edit mode only-->
		<SharePointWebControls:CssRegistration name="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/editmode15.css %>"
			After="<% $SPUrl:~sitecollection/Style Library/~language/Themable/Core Styles/pagelayouts15.css %>" runat="server"/>
        <style type="text/css">
            #wPageTitle {
                display: none;
            }
        </style>
	</PublishingWebControls:EditModePanel>
	<SharePointWebControls:FieldValue id="PageStylesField" FieldName="HeaderStyleDefinitions" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitle" runat="server">
	<SharePointWebControls:FieldValue id="PageTitle" FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePointWebControls:FieldValue FieldName="Title" runat="server"/>
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderTitleBreadcrumb" runat="server"> 
	<SharePointWebControls:ListSiteMapPath runat="server" SiteMapProviders="CurrentNavigationSwitchableProvider" RenderCurrentNodeAsLink="false" PathSeparator="" CssClass="s4-breadcrumb" NodeStyle-CssClass="s4-breadcrumbNode" CurrentNodeStyle-CssClass="s4-breadcrumbCurrentNode" RootNodeStyle-CssClass="s4-breadcrumbRootNode" NodeImageOffsetX=0 NodeImageOffsetY=289 NodeImageWidth=16 NodeImageHeight=16 NodeImageUrl="/_layouts/15/images/fgimg.png?rev=23" HideInteriorRootNodes="true" SkipLinkText=""/> </asp:Content>
<asp:Content ContentPlaceholderID="PlaceHolderMain" runat="server">
<div class="row">
		<div class="col-md-12">
            <div class="page-actions">
                <!--<div class="action pull-right" onclick="javascript:quickLinksQuickAdd();"><span class="glyphicon glyphicon-plus"></span> Add to my Quick Links</div>-->
                <div class="action pull-right" id="likeButton" onclick="javascript:(function() {likepage.LikePage();})()" title="Click here to like this page."><span class="glyphicon glyphicon-thumbs-up"></span> Like!  <span class="badge"></span></div>
                
            </div>
        </div>
         
    </div>
    <div class="row">
		<div class="col-md-12">
            <div>
				<WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="Header" Title="loc:Header" FrameType="None" Orientation="Vertical">
                    <ZoneTemplate></ZoneTemplate>
				</WebPartPages:WebPartZone>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <h1 id="wPageTitle"><SharePointWebControls:TextField runat="server" FieldName="Title" DisableInputFieldLabel/></h1>
            <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit">
			        <SharePointWebControls:TextField runat="server" FieldName="Title"/>
            </PublishingWebControls:EditModePanel>
            <div class="article article-left">
		        <div class="col-md-9">
		            <div class="article-header">
			            <div class="date-line">
				            <SharePointWebControls:DateTimeField FieldName="ArticleStartDate" runat="server"/>
			            </div>
			            <div class="by-line">
				            <SharePointWebControls:TextField FieldName="ArticleByLine" runat="server"/>
			            </div>
		            </div><br />
		            <div class="article-content">
			            <PublishingWebControls:RichHtmlField FieldName="PublishingPageContent" HasInitialFocus="True" MinimumEditHeight="400px" runat="server"/>
		            </div>
		            <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel roll-up">
			            <PublishingWebControls:RichImageField FieldName="PublishingRollupImage" AllowHyperLinks="false" runat="server" />
			            <asp:Label text="<%$Resources:cms,Article_rollup_image_text15%>" CssClass="ms-textSmall" runat="server" />
		            </PublishingWebControls:EditModePanel>
                </div>
                <div class="col-md-3">
		            <div class="captioned-image">
			            <div class="image">
				            <PublishingWebControls:RichImageField FieldName="PublishingPageImage" runat="server"/>
			            </div>
			            <div class="caption">
				            <PublishingWebControls:RichHtmlField FieldName="PublishingImageCaption"  AllowTextMarkup="true" AllowTables="false" AllowLists="false" AllowHeadings="false" AllowStyles="false" AllowFontColorsMenu="false" AllowParagraphFormatting="false" AllowFonts="false" PreviewValueSize="Small" AllowInsert="false" AllowEmbedding="false" AllowDragDrop="false" runat="server"/>
			            </div>
		            </div>
                    
                    <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="SideBarZone" Title="Side Bar" FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
                    
                </div>
	        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="LeftColumnMiddle" Title="Left Column Middle" FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
        <div class="col-md-4">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="MiddleColumnMiddle" Title="Middle Column Middle"  FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
        <div class="col-md-4">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="RightColumnMiddle" Title="Right Column Middle" FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="LeftColumnBottom" Title="Left Column Bottom" FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>
        <div class="col-md-4">
            <WebPartPages:WebPartZone runat="server" AllowPersonalization="false" ID="RightColumnBottom" Title="Right Column Bottom"  FrameType="None" Orientation="Vertical"><ZoneTemplate></ZoneTemplate></WebPartPages:WebPartZone>
        </div>       
    </div>
    <div class="row">
        <hr />
        
        <div class="col-md-12" id="comments-container">
            <h4 style="display: none;">Comments</h4>
            <div id="comments" style="display: none;"></div>
            <a id="seeAllComments" style="display:none;" class="pull-right" href="javascript:getMoreComments();">See all comments</a>
            <h4>Leave a comment...</h4>
            <textarea name="comment" title="Enter your comment here..." id="comment" style="margin-bottom: 5px;width: 100%; height: 70px; overflow: hidden; -ms-overflow-y: hidden; -ms-word-wrap: break-word;" placeholder="Enter your comment here..." maxlength="254"></textarea>
            <span id="commentMessage" class="alert-xs alert-danger" style="display: none;">You must enter a comment first</span>
            <a class="btn btn-primary comment-submit pull-right" onclick="javascript:postComment();">Submit</a>        
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <PublishingWebControls:EditModePanel runat="server" CssClass="edit-mode-panel title-edit publishing-options-container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Featured Article</span>
                            <SharePointWebControls:BooleanField runat="server" FieldName="FeaturedNews" DisableInputFieldLabel/>
                            <small>Shows this article in homepage slider.</small><br>
                        </div>
                    </div>
                
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Promoted Tile</span>
                            <SharePointWebControls:BooleanField runat="server" FieldName="PromotedTile" DisableInputFieldLabel/>
                            <small>Shows this article in the homepage tile section.</small><br>
                        </div>
                    </div>
                
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Sort Order</span>
                            <SharePointWebControls:NumberField runat="server" FieldName="SortOrder" DisableInputFieldLabel/>
                            <small>What position should this article appear in?</small><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">News Category</span>
                            <SharePointWebControls:DropDownChoiceField runat="server" FieldName="NewsType" DisableInputFieldLabel/>
                            <small>What news category does this fall into?</small><br>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Removal Date</span>
                            <SharePointWebControls:DateTimeField runat="server" FieldName="RemovalDate" DisableInputFieldLabel/>
                            <small>Is there are 'definitive' removal date for this article?</small><br>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Overlay Banner Color</span>
                            <SharePointWebControls:TextField runat="server" FieldName="OverlayBannerColor" DisableInputFieldLabel/>
                            <small>What color should the overlay banner be? Must be HEX or RGB color code.</small><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Teaser Text</span>
                            <SharePointWebControls:TextField runat="server" FieldName="TeaserText" DisableInputFieldLabel/>
                            <small>If this is a promoted tile, use up to 60 characters to 'tease' this article.</small><br>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Comments</span>
                            <SharePointWebControls:NoteField runat="server" FieldName="Comments" DisableInputFieldLabel/>
                            <small>Use the comments feild to show rollup text for announcements.</small><br>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Office</span>
                            <SharePointWebControls:CheckBoxChoiceField runat="server" FieldName="WellmarkOffice" DisableInputFieldLabel/>
                            <small>Target a certain office.</small><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="publishing-control publishing-options">
                            <span class="control-title">Teaser HTML</span> (<a style="cursor:pointer;" onclick="javascript:showNewsBannerPreview();">PREVIEW</a>)
                            <PublishingWebControls:RichHtmlField FieldName="Teaser" DisableInputFieldLabel AllowTextMarkup="false" AllowTables="false" AllowLists="false" AllowHeadings="true" AllowStyles="true" AllowFontColorsMenu="true" AllowParagraphFormatting="true" AllowFonts="true" PreviewValueSize="Small" AllowInsert="false" AllowEmbedding="false" AllowDragDrop="false" runat="server"/>
                            <small>If this is a featured article you can create your own HTML to be displayed on the slide.</small><br>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner-preview" style="display: none; background-color: #0079bc; height: 300px; width: 100%">

                        </div>
                    </div>
                </div>

		    </PublishingWebControls:EditModePanel>
        </div>
    </div>
</asp:Content>
