function checkSiteAccess() {
    $('.nav-link').each(function(){
        var e = this;
        console.log(e);
        $.ajax({
            url: $(e).attr('href'), 
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                //Everything is good
            },
            error: function (data) {
                $(e).hide();
            }
        });
    });

}