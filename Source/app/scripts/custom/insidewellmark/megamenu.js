﻿// This Library is used to render the MegaMenu links into their container on the master page. Overall operation is outlined below.


//1. Read from list and get sorted JSON data with all links, groups, etc.
//2. Use jQuuery to insert them into the Nav on page load.


//START:MegaMenu functions
function getMegaMenuList() {
        //Get items   
        $.ajax({
            url: "/_api/web/lists/getbytitle('MegaMenu')", //Use OData operators such as $Select=Title eq 'My Title'
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {

                //If mega menu list is present then build get the items and build the menu
                getMegaMenuListItems();

            },
            error: function (data) {

                //If the menu does not exist add a button or the user to create the list.
                $('.navbar.navbar-default.navbar-static-top .nav.navbar-nav').append('<li style="cursor: pointer;" class="dropdown menu-large"><a onclick="javascript:createMegaMenuList();" class="dropdown-toggle" data-toggle="dropdown">No mega menu list found</a><li>');

            }
        });
    }

//START:Get menu items and build menu 
function getMegaMenuListItems() {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('MegaMenu')/items?$filter=Active eq '1'&$orderby=Parent,SortOrder&$top=500", 
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Build the HTML for the mega menu
            buildMegaMenu(data.d);
        },
        error: function (data) {
            //debugger;
        }
    });

}

function buildMegaMenu(data) {

    var i;
    var c;
    

    var topLevelItems = $.grep(data.results, function (item) { return item.ParentId == null }); //These items appear on the NAV bar
    //var groups = $.grep(data.results, function (item) { return item.URL == null && item.ParentId != null });
    var groups = $.grep(data.results, function (item) { return item.ParentId != null });
    //Add home icon
    //$('.navbar.navbar-default.navbar-static-top .nav.navbar-nav').append('<li class="dropdown menu-large"><a class="dropdown-toggle" href="/" data-toggle="dropdown"><span class="glyphicon glyphicon-home"></span></a></li>');
    for (i = 0; i < topLevelItems.length; i++) {

        //var topLevelChildItems = $.grep(data.results, function (item) { return item.ParentId == topLevelItems[i].Id });
        
        $('.navbar.navbar-default.navbar-static-top .nav.navbar-nav').append(
            '<li class="dropdown menu-large"  id="topLevel_' + topLevelItems[i].Id + '">\
	            <a href="" class="dropdown-toggle" data-toggle="dropdown">' + topLevelItems[i].Title + ' <b class="glyphicon glyphicon-chevron-down" aria-hidden="true"></b></a>\
		            <ul class="dropdown-menu megamenu row">\
			            <li class="col-sm-3 col-1">\
				            <ul>\
					            <!-- Items are created here via jQuery within the MegaMenu.js library -->\
				            </ul>\
			            </li>\
			            <li class="col-sm-3 col-2">\
				            <ul>\
					            <!-- Items are created here via jQuery within the MegaMenu.js library -->\
				            </ul>\
			            </li>\
			            <li class="col-sm-3 col-3">\
				            <ul>\
					            <!-- Items are created here via jQuery within the MegaMenu.js library -->\
				            </ul>\
			            </li>\
			            <li class="col-sm-3 col-4">\
				            <ul>\
					            <!-- Items are created here via jQuery within the MegaMenu.js library -->\
				            </ul>\
			            </li>\
		            </ul>\
            </li>'
        );

        //for (t = 0; t < singleItems.length; t++) {

        //    if (singleItems[t].Divider == false) {

        //        $('#topLevel_' + topLevelItems[i].Id + ' .col-' + singleItems[t].Column + ' ul').append('<li><a href="' + singleItems[t].URL + '">' + singleItems[t].Title + '</a></li>');

        //    } else {

        //        $('#topLevel_' + topLevelItems[i].Id + ' .col-' + singleItems[t].Column + ' ul').append('<li><a href="' + singleItems[t].URL + '">' + singleItems[t].Title + '</a></li><li class="divider"></li>');

        //    }
        //}
    }   

    for (i = 0; i < groups.length; i++) {

        var target;
        if (groups[i].Target == 'New Window' ) { target = "_blank"; } else { target = "_self"; }
        var groupChildItems = $.grep(data.results, function (item) { return item.ParentId == groups[i].Id });
        
        if (groupChildItems.length === 0) {

            if (groups[i].Divider == false) {

                $('#topLevel_' + groups[i].ParentId + ' .col-' + groups[i].Column + ' ul').append('<li><a class="megamenu-item" href="' + groups[i].URL + '" target="' + target + '">' + groups[i].Title + '</a></li>');

            } else {

                $('#topLevel_' + groups[i].ParentId + ' .col-' + groups[i].Column + ' ul').append('<li><a class="megamenu-item" href="' + groups[i].URL + '" target="' + target + '">' + groups[i].Title + '</a></li><li class="divider"></li>');

            }

        } else {

            if (groups[i].Divider == false) {

                $('#topLevel_' + groups[i].ParentId + ' .col-' + groups[i].Column + ' ul').append('<li class="dropdown-header" id="group_' + groups[i].Id + '"><a href="' + groups[i].URL + '" target="' + target + '">' + groups[i].Title + '</a></li>');

            } else {

                $('#topLevel_' + groups[i].ParentId + ' .col-' + groups[i].Column + ' ul').append('<li id="group_' + groups[i].Id + '"><a href="' + groups[i].URL + '" target="' + target + '">' + groups[i].Title + '</a></li><li class="divider"></li>');

            }

            for (c = groupChildItems.length -1; c >= 0; c--) {
                if (groupChildItems[c].Target == 'New Window') { target = "_blank"; } else { target = "_self"; }
                if (groupChildItems[c].Divider == false) {

                    $('#group_' + groups[i].Id).after('<li><a href="' + groupChildItems[c].URL + '" target="' + target + '">' + groupChildItems[c].Title + '</a></li>');

                } else {

                    $('#group_' + groups[i].Id).after('<li><a href="' + groupChildItems[c].URL + '" target="' + target + '">' + groupChildItems[c].Title + '</a></li><li class="divider"></li>');

                }
            }
        }
    }
}
//END:Get menu items and build menu 


    
