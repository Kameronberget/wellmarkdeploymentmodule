﻿//This script library contains functions needed to support DOM manipulation and custom elements in the intranet.

//START:Load welcome message
function initializePage() {

    //Get user info
    getMyProperties();

    // This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
    //var context = SP.ClientContext.get_current();
    //var user = context.get_web().get_currentUser();

    var today = new Date();
    var monthNumber = today.getMonth() + 1;
    var date = today.getDate();
    var dayNumber = today.getDay();
    var day;
    var month;
    var year = today.getFullYear();

    if (dayNumber == 0) { day = 'Sunday'; }
    if (dayNumber == 1) { day = 'Monday'; }
    if (dayNumber == 2) { day = 'Tuesday'; }
    if (dayNumber == 3) { day = 'Wednesday'; }
    if (dayNumber == 4) { day = 'Thursday'; }
    if (dayNumber == 5) { day = 'Friday'; }
    if (dayNumber == 6) { day = 'Saturday'; }

    if (monthNumber == 1) { month = 'January'; }
    if (monthNumber == 2) { month = 'February'; }
    if (monthNumber == 3) { month = 'March'; }
    if (monthNumber == 4) { month = 'April'; }
    if (monthNumber == 5) { month = 'May'; }
    if (monthNumber == 6) { month = 'June'; }
    if (monthNumber == 7) { month = 'July'; }
    if (monthNumber == 8) { month = 'August'; }
    if (monthNumber == 9) { month = 'September'; }
    if (monthNumber == 10) { month = 'October'; }
    if (monthNumber == 11) { month = 'November'; }
    if (monthNumber == 12) { month = 'December'; }
    $('#welcomeDate').html(day + ', ' + month + ' ' + today.getDate() + ', ' + year);

    //context.load(user);
    //context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
    $('.unslider-nav li').each(function () {

        var count = 4;
        $(this).attr('tabindex', '0');
        count++;
    });
    $('.unslider-nav').attr('tabindex', '0');
    $('.ms-srch-sb-navLink').attr('role', 'menuitem');
    $('.ms-srch-sb-navLink').click(function () {
        var placeholder = $('#SearchBox input').attr('Title');
        $(this).attr('aria-valuetext', placeholder)
    });
}

//This function is executed if the above call is successful. It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    
    if (user.get_title() === 'System Account') {
        $('#welcomeMessage').html('<span style="color:red !Important">YOU ARE USING A SYSTEM ACCOUNT</span>');
    } else {
        $('#welcomeMessage').html('Hello, ' + user.get_title() + '');
    }
    
}

//This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    $('#welcomeMessage').text('Failed to get user\'s name');

}

//This function will get list items from sharepoint lists.
function getListItems(listName, query, successHandler) {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/items" + query, //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            successHandler(data.d);
            
        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

function getFolder(folderUrl, query, successHandler) {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/getfolderbyserverrelativeurl('" + folderUrl + "')/files" + query, //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            successHandler(data.d);

        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

function getListItemsSvc(listName, query, successHandler) {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_vti_bin/ListData.svc/" + listName + query, //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            successHandler(data.d);

        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

function getRegistrationsForEvent(eventId) {

    var query = "?$Filter=EventId eq '" + eventId + "'";
    var results;

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Registrations')/items" + query, //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            results = data.d;

        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

    return results;
}

function buildIconTable(data) {
    var html = '';
    var i;

    for (i = 0; i < data.results.length; i++) {

        if (data.results[i].ServerRelativeUrl == $('input[title="Icon URL"]').val()) {
            html += '<div class="icon"><img class="selected" onclick="javascript:setIconUrl(this);" src="' + data.results[i].ServerRelativeUrl + '" alt="' + data.results[i].Title + '"/></div>'
        } else {
            html += '<div class="icon"><img onclick="javascript:setIconUrl(this);" src="' + data.results[i].ServerRelativeUrl + '" alt="' + data.results[i].Title + '"/></div>'
        }

    }

    $('.iconContainer').html(html);
}

function showIconImage(data) {
    $('.iconContainer').html('<div class="icon"><img src="' + data.d.IconURL + '" alt="' + data.d.Title + '" /></div>');
}

function setIconUrl(e) {

    var url = $(e).attr('src');
    $('input[title="Icon URL"]').val(url);
    $('.iconContainer div').each(function () {
        $(this).find('img').removeClass('selected');
    });
    $(e).addClass('selected');
}

function getPollResponses(pollTitle) {

    var responses;

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Poll Responses')/items?$filter=Title eq '" + pollTitle + "'", 
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            responses = data.d;
            

        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

    return responses;

}

//Get single item
function getPollResponse(id) {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Polls')/items(" + id + ")", //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            $('.poll-message').html('It looks like you have already answered this poll.');

        },
        error: function (data) {
            submitPoll();
        }
    });

}

//Add list item
function addListItem(listName, metadata, successHandler) {

    //Prepping our update
    //Metadata object needs to contain columns and values like var metadata = { 'Title': Value; };
    var item = $.extend({
        "__metadata": { "type": "SP.Data." + listName[0].toUpperCase() + listName.substring(1).replace(/\s+/g, '') + "ListItem" }
    }, metadata);

    // Executing our add
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + listName + "')/items",
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify(item),
        headers: {
            "Accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            //Write confirmation message to screen for user using the addMessage function
            successHandler(data);
        },
        error: function (data) {
            //Write error message to screen for user using the addMessage function;
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

//Get the current user's location profile data
function getMyProperties() {
    //Get user profile location 
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties", //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //Do something with items. Items come back in data.d.results object. Count on data.d.results.length
            //console.log(data);
            
            $('#welcomeMessage').html('Hello, <a class="user-name" href="' + data.d.PersonalUrl + '">' + data.d.UserProfileProperties.results[4].Value + ' ' + data.d.UserProfileProperties.results[6].Value + '</a>');
            var location = data.d.UserProfileProperties.results[58].Value;
            var office = 'DM';
            if (location === null || location == '') { office = "DM"; }
            if (location.indexOf('DM') > 0) { office = "DM"; }
            if (location.indexOf('SF') > 0) { office = "SF"; }
            if (location.indexOf('CR') > 0) { office = "CR"; }

            getListItems('Weather', "?$filter=Title eq '" + office + "'", displayWeather);
        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });
}

function getMyPropertiesAsync() {

    var location;

    //Get user profile location 
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties", //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

          
            location = data.d.UserProfileProperties.results[58].Value;
            
        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

    return location;
}

//Show the weather data from teh SP list into the Weather DIV
function displayWeather(data) {

    var t;
    var hour = new Date().getHours();
    if (hour >= 17) { t = 'night'; } else { t = 'day'; }
    var icon;

    switch (data.results[0].Icon) {
        case 'chanceflurries': {
            icon = 'snow.png';
            break;
        }
        case 'chancerain': {
            icon = 'rain.png';
            break;
        }
        case 'chancesleet': {
            icon = 'rain.png';
            break;
        }
        case 'chancesnow': {
            icon = 'snow.png';
            break;
        }
        case 'chancestorms': {
            icon = 'thunderstorms.png';
            break;
        }
        case 'clearday': {
            icon = 'day_clear.png';
            break;
        }
        case 'clearnight': {
            icon = 'night_clear.png'
            break;
        }
        case 'flurry': {
            icon = 'snow.png';
            break;
        }
        case 'fog': {
            icon = 'fog.png';
            break;
        }
        case 'hazy': {
            icon = 'fog.png';
            break;
        }
        case 'mostlycloudy': {
            icon = 'clouds.png';
            break;
        }
        case 'mostlycloudyday': {
            icon = 'clouds.png';
            break;
        }
        case 'mostlycloudynight': {
            icon = 'clouds.png';
            break;
        }
        case 'mostlysunny': {
            icon = 'day_clear.png';
            break;
        }
        case 'partlycloudy': {
            icon = 'clouds.png';
            break;
        }
        case 'partlysunny': {
            icon = 'fewclouds.png';
            break;
        }
        case 'sleet': {
            icon = 'rain.png';
            break;
        }
        case 'rain': {
            icon = 'rain.png';
            break;
        }
        case 'snow': {
            icon = 'snow.png';
            break;
        }
        case 'sunny': {
            icon = 'day_clear.png';
            break;
        }
        case 'tstorms': {
            icon = 'thunderstorms.png';
            break;
        }
        case 'cloudy': {
            icon = 'clouds.png';
            break;
        }
        default: {
            icon = 'day_clear.png';
            break;
        }
    }

    if ($('.weather').length > 0) {
        if (data.results.length === 0) {
            $('.weather').html('No weather data found');
        } else {
            $('.weather').html(
                '<div class="weather-temp">\
		        <img src="/SiteCollectionImages/' + icon + '" />\
		        ' + data.results[0].Temperature + '&deg;</div>\
		        <div class="weather-desc">' + data.results[0].Description + '<br/><small>' + data.results[0].City + '</small><i><a class="pull-right weather-ext" href="' + data.results[0].Website + '" target="_blank">Extended Forecast</a></i></div>\
            ');
        }
    }

}


//POLLING

//Step 1: Get current poll and either display poll or chart

function getActivePoll() {

    //Get items   
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Polls')/items?$orderby=Modified desc&$filter=Active eq 1", //If multiple polls are active, this query will grab the msot recent
        method: "GET",
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            if (data.d.results.length === 0) {
                $('.poll').hide();
            } else {
                buildPoll(data.d);
            }

        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

function buildPoll(data) {

    var responses = getPollResponses(data.results[0].Title);
    var myResponse = $.grep(responses.results, function (item) { return item.EditorId === _spPageContextInfo.userId });

    if (myResponse.length === 0) {
        var i;
        var html = '';
        var choices = data.results[0].Choices.split(',');

        //Fill in the question
        $('.poll-question').text(data.results[0].Title);



        for (i = 0; i < choices.length; i++) {

            html += '<div class="radio">\
                <label>\
                <input type="radio" role="radio" name="poll_choice" value="' + choices[i] + '"  tabindex="0">\
                ' + choices[i] + '\
                </label>\
            </div>';
        }

        if (data.results[0].Answer != null) {
            $('.poll-content').attr("data-answer", data.results[0].Answer);
        }
        //Add metdata as data attributes to the poll
        $('.poll-content').attr("data-pollId", data.results[0].Id);
        $('.poll-content').attr("data-polldata", data.results[0].PollData);
        $('.poll-content .btn').attr('tabindex', '0')
        //Add Choices to DOM
        $('.poll-question').after(html);

        //Show the poll
        $('.poll').show();
        $('.poll-content').show();

    } else {
        generateResultsGraph(responses);
    }
}

function generateResultsGraph(data) {


    var i;
    var chartData = []; //JSON.parse(data.results[0].PollData);
    var c;
    var colorIdx = 0;
    var legendHtml = '';
    var exists;
    var colors = [];
    colors.push('#0079bc');
    colors.push('#d87927');
    colors.push('#579099');
    colors.push('#97a03f');
    colors.push('#d2a01f');
    colors.push('#55a0d3');
    colors.push('#b37129');
    colors.push('#bb6183');
    colors.push('#861a52');
    colors.push('#569bbe');

    for (i = 0; i < data.results.length; i++) {
        exists = false;
        if (chartData.length === 0) {
            chartData.push({ "value": 1, "label": data.results[i].Response, "color": colors[colorIdx], "labelColor": "white", "labelFontSize": "16" });
            colorIdx++;
        } else {
            for (c = 0; c < chartData.length; c++) {
                if (chartData[c].label == data.results[i].Response.toString()) {
                    chartData[c].value++;
                    exists = true;
                } 
            }
            if (exists == false) {
                chartData.push({ "value": 1, "label": data.results[i].Response, "color": colors[colorIdx], "labelColor": "white", "labelFontSize": "16" });
                colorIdx++;
            }
        }
    }

    var options = {

        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke: true,

        //String - The colour of each segment stroke
        segmentStrokeColor: "#fff",

        //Number - The width of each segment stroke
        segmentStrokeWidth: 2,

        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts

        //Number - Amount of animation steps
        animationSteps: 100,

        //String - Animation easing effect
        animationEasing: "easeOutBounce",

        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,

        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,

        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

    }
    //$('.poll').append('<canvas id="poll_graph"></canvas>');
    //$('.poll').append('<div class="chart-legend"></div>');

    $('.poll-content').hide();
    var ctx = document.getElementById("poll_graph").getContext("2d");

    // For a pie chart
    //var myPieChart = new Chart(ctx[0]).Pie(chartData, options);

    // For a doughnut chart
    $('.poll-title').after('<span class="poll-question-answered">' + data.results[0].Title + '</span>');
    var myPieChart = new Chart(ctx).Pie(chartData, options);
    var legend = myPieChart.generateLegend();
    $(".poll-legend").html(legend);

    //Generate Hover
    $('.poll').hover(function () {
        $('.poll-title').html('Just Wondering  <span class="glyphicon glyphicon-user"></span> <small><a style="color: #fff; text-decoration: none; text-transform: none;" href="/Lists/PollResponses">See my poll responses</a></small>');
    },
    function () {
        $('.poll-title').html('Just Wondering');
    }
    );

}

//Step 2: Submit Poll 

function submitPoll() {

    //Submit opinion poll
    if ($('.poll-content').data("answer") == null) {

        if ($('input[name=poll_choice]:checked').val() == null) {
            $('.poll-message').html('<div class=""alert alert-danger">Please choose an option first.</div>')
            return;
        } else {
            //var pollData = JSON.parse($('.poll-content').data("polldata"));
            var metadata = {
                Title: $('.poll-question').text(),
                Response: $('input[name=poll_choice]:checked').val()
            }

            addPollResponse(metadata);
            
        }
    } else {
        //Submit answer poll
        var answer;

        if ($('input[name=poll_choice]:checked').val() == $('.poll-content').data('answer')) {

            $('.poll-options').hide();
            $('.poll-message').show();
            $('.poll-button').hide();
            $('.poll-message').html('<div class="answer"><img src="/SiteCollectionImages/Correct_Icon.png?RenditionId=1" alt="Correct Answer"/><br>Correct!</div>');
            //setTimeout(function () { getActivePoll(); }, 5000);
        } else {

            $('.poll-options').hide();
            $('.poll-message').show();
            $('.poll-button').hide();
            $('.poll-message').html('<div class="answer"><img src="/SiteCollectionImages/Incorrect_Icon.png?RenditionId=1" alt="Correct Answer"/><br>Incorrect!</div>');
            setTimeout(function () { $('.poll-options').show(); $('.poll-message').hide(); $('.poll-button').show(); }, 5000);
        }
    }
}

function addPollResponse(metadata) {

    //Prepping our update
    //Metadata object needs to contain columns and values like var metadata = { 'Title': Value; };
    var item = $.extend({
        "__metadata": { "type": "SP.Data.PollResponsesListItem" }
    }, metadata);

    // Executing our add
    $.ajax({
        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Poll Responses')/items",
        type: "POST",
        contentType: "application/json;odata=verbose",
        data: JSON.stringify(item),
        headers: {
            "Accept": "application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function (data) {
            //Write confirmation message to screen for user using the addMessage function
            getActivePoll(metadata);
        },
        error: function (data) {
            //Write error message to screen for user using the addMessage function;
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

}

//function updatePollData(metadata) {

//    var title = metadata.Title;
//    var response = metadata.Response;
//    var chartData = [];

//    //Get items   
//    $.ajax({
//        url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Poll Responses')/items?$filter=Title eq '" + title + "'", 
//        method: "GET",
//        headers: { "Accept": "application/json; odata=verbose" },
//        success: function (data) {

//            var i;

//            var responseData = $.grep(chartData, function (item) { return item.label == response });

//            if (responseData.length === 0 || chartData.length === 0) {

//                chartData.push({ value: 1, label: response });

//            } else {

//                for (i = 0; i < chartData.length; i++) {

//                    if (chartData[i].label == response) {

//                        chartData[i].value++;

//                    }

//                }
//            }

//            var metadata = { PollData: JSON.stringify(chartData) }
//            updateListItem(_spPageContextInfo.siteAbsoluteUrl, "Polls", data.d.results[0].Id, metadata);
//            //generateResultsGraph(data);

//        },
//        error: function (data) {
//            addStatusMessage("Error: " + data.responseText, false);
//        }

//    });

//}


//CRUD FUNCTIONS

function getListItem(url, listname, id, complete, failure) {
        // Getting our list items
        $.ajax({
            url: url + "/_api/web/lists/getbytitle('" + listname + "')/items(" + id + ")",
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                // Returning the results
                complete(data);
            },
            error: function (data) {
                console.log(data.responseText);
            }
        });
    }

function updateListItem(url, listname, id, metadata) {

        // Prepping our update
        var item = $.extend({
            "__metadata": { "type": "SP.Data." + listname[0].toUpperCase() + listname.substring(1).replace(/\s+/g, '') + "ListItem" }
        }, metadata);

        getListItem(url, listname, id, function (data) {
            $.ajax({
                url: data.d.__metadata.uri,
                type: "POST",
                contentType: "application/json;odata=verbose",
                data: JSON.stringify(item),
                headers: {
                    "Accept": "application/json;odata=verbose",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "X-HTTP-Method": "MERGE",
                    "If-Match": data.d.__metadata.etag
                },
                success: function (data) {
                    getActivePoll();
                },
                error: function (data) {
                    
                }
            });

        }, function (data) {
            console.log(data.responseText);
        });

    }

function checkRegistration(e) {
    getListItems("Registrations", "?$Filter=((EventId eq '" + $(e).data("eventid") + "') and (Author eq '" + _spPageContextInfo.userId + "'))", function (data) {
        if (data.results.length <= 0) {
            $(e).find('.register-button').show();
        }
    });
}


//QUICK LINKS
function getQuickLinks() {

    var lists = ["Quick Links Admin", "Quick Links User"];
    var l;
    var i;
    var quickLinks = [];
    var url;

    for (l = 0; l < lists.length; l++) {

        if (lists[l] == "Quick Links Admin") {
            url = _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + lists[l] + "')/items?$orderby=SortOrder";
        } else {
            url = _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + lists[l] + "')/items?$filter=Author eq '" + _spPageContextInfo.userId + "'&$orderby=SortOrder";
        }
            $.ajax({
                url: url,
                method: "GET",
                headers: { "Accept": "application/json; odata=verbose" },
                async: false,
                success: function (data) {
                    for (i = 0; i < data.d.results.length; i++) {
                        data.d.results[i].Fromlist = lists[l];
                        quickLinks.push(data.d.results[i]);
                    }
                },
                error: function (data) {
                    addStatusMessage("Error: " + data.responseText, false);
                }
            });
        

    }

    buildQuickLinks(quickLinks);
    
}

function getPublishingImage(uri) {
    var publishingImage;

    $.ajax({
        url: uri,
        method: 'GET',
        async: false,
        headers: {
            Accept: 'application/json; odata=verbose'
        },
        success: function (data, request) {
            publishingImage = data.d.PublishingRollupImage;
        }
    });

    return publishingImage;
}

function buildQuickLinks(data) {

    var html = '';
    var i;
    var e;
    var emptySlots;

    var publishingImage;
    if (data.length < 10) {
        emptySlots = (10 - data.length);
    } else {
        emptySlots = 1;
    }

    html += '<ul class="touchcarousel-container">';

    for (i = 0; i < data.length; i++) {

        var url;
        if (data[i].URL === null) { url = '#'; } else { url = data[i].URL.Url; }
        
        
        //publishingImage = getPublishingImage(data[i].FieldValuesAsHtml.__deferred.uri);


        if (data[i].Fromlist == "Quick Links User") {
            if (data[i].Target == 'Current Window') {
                html += '<li class="touchcarousel-item user-link"><div style="display:none;" class="quicklink-edit" onclick="javascript:editQuickLink(\'Edit Quick Link\', \'/Lists/QuickLinksUser/EditForm.aspx?Source=/&IsDlg=1&ID=' + data[i].Id + '\');">Edit</div><a class="quicklink-link" href="' + url + '"><img src="' + data[i].IconURL + '" alt="' + data[i].Title + '" /><br/>' + data[i].Title + '</a></li>';
            } else {
                html += '<li class="touchcarousel-item user-link"><div style="display:none;" class="quicklink-edit" onclick="javascript:editQuickLink(\'Edit Quick Link\', \'/Lists/QuickLinksUser/EditForm.aspx?Source=/&IsDlg=1&ID=' + data[i].Id + '\');">Edit</div><a class="quicklink-link" href="' + url + '" target="_blank"><img src="' + data[i].IconURL + '" alt="' + data[i].Title + '" /><br/>' + data[i].Title + '</a></li>';
            }
        } else {
            if (data[i].Target == 'Current Window') {
                html += '<li class="touchcarousel-item"><a class="quicklink-link" href="' + url + '"><img src="' + data[i].IconURL + '" alt="' + data[i].Title + '" /><br/>' + data[i].Title + '</a></li>';
            } else {
                html += '<li class="touchcarousel-item"><a class="quicklink-link" href="' + url + '" target="_blank"><img src="' + data[i].IconURL + '" alt="' + data[i].Title + '" /><br/>' + data[i].Title + '</a></li>';
            }
        }


    }

    for (e = 0; e < emptySlots; e++) {
        html += '<li class="touchcarousel-item"><a href="javascript:showModalDialog(\'Add Personal Link\', \'/Lists/QuickLinksUser/NewForm.aspx?Source=/&IsDlg=1\');"><img src="/Icons/AddQuickLink.png" alt="Add your own link" /><br/>Add a link</a></li>';
    }

    
    html += '</ul>';
    //console.log(html);
    $('#wellmarkCarousel').html(html);    
    $("#wellmarkCarousel").touchCarousel({
        itemsPerMove: 2,
        snapToItems: true,
        pagingNav: true,
        pagingNavControls: false,
        autoplay: false,
        autoplayDelay: 3000,
        autoplayStopAtAction: true,
        scrollbar: false,
        scrollbarAutoHide: false,
        scrollbarTheme: "dark",
        transitionSpeed: 600,
        directionNav: true,
        directionNavAutoHide: false,
        loopItems: false,
        keyboardNav: false,
        dragUsingMouse: true,
        scrollToLast: false,
        itemFallbackWidth: 500,
        baseMouseFriction: 0.0012,
        baseTouchFriction: 0.0008,
        lockAxis: true,
        useWebkit3d: false,
        onAnimStart: null,
        onAnimComplete: null,
        onDragStart: null,
        onDragRelease: null
    });


    ////Update Alt tags and screen reader options
    //$('.arrow-icon.left').attr('alt', 'Move quick links slider to left');
    //$('.arrow-icon.right').attr('alt', 'Move quick links slider to right');
    //$('.arrow-icon').attr('aria-hidden', 'true');


    $('.touchcarousel-item.user-link').hover(
	    function () {
	        $(this).find('.quicklink-edit').show();
	        $(this).find('.quicklink-link').addClass('quicklink-edit-fix');	        
	    },
	    function () {
	        $(this).find('.quicklink-edit').hide();
	        $(this).find('.quicklink-link').removeClass('quicklink-edit-fix');
	    }
    );

    $('.arrow-holder.left').attr('aria-disabled', 'true');

    $('.arrow-holder').click(function () {

        if ($(this).hasClass('disabled') == true) {
            $(this).attr('aria-disabled', 'true');
        } else {
            $(this).attr('aria-disabled', 'false');
        }
    });
    
}

//SLIDER

function controlAutoplay(action) {
    // Getting instance
    var sliderInstance = $("#wellmarkCarousel").data("touchCarousel");
    if (action === true) {
        $('.banner').unslider('start');
        $('.glyphicon.glyphicon-play').hide()
        $('.glyphicon.glyphicon-pause').show()
    } else {
        $('.banner').unslider('stop');
        $('.glyphicon.glyphicon-pause').hide()
        $('.glyphicon.glyphicon-play').show()
    }
}

function getEvents() {
    getListItems('Events', '?$filter', buildEventTiles);
    
    //var today = new Date();
    //getListItemsSvc('Events', '?$filter=StartTime+ge+datetime\'' + today.toISOString() + '\'', buildEventTiles);
}

function buildEventTiles(data) {

    var dayCount = 5;
    var d;
    var i;
    var html = '';
    var today = new Date();
    var date = today.getDate();
    var month;
    var color1;
    var color2;
    
    

    switch (today.getMonth()) {
        case 0:
            month = "Jan";
            currentMonthColor1 = '#3EB1C8';
            currentMonthColor2 = '#77C5D5';
            break;
        case 1:
            month = "Feb";
            currentMonthColor1 = '#8C4799';
            currentMonthColor2 = '#A15A95';
            break;
        case 2:
            month = "Mar";
            currentMonthColor1 = '#E07E3C';
            currentMonthColor2 = '#FF7F41';
            break;
        case 3:
            month = "Apr";
            currentMonthColor1 = '#6FA287';
            currentMonthColor2 = '#A9C47F';
            break;
        case 4:
            month = "May";
            currentMonthColor1 = '#00558C';
            currentMonthColor2 = '#0086BF';
            break;
        case 5:
            month = "Jun";
            currentMonthColor1 = '#E04E39';
            currentMonthColor2 = '#E56A54';
            break;
        case 6:
            month = "Jul";
            currentMonthColor1 = '#3EB1C8';
            currentMonthColor2 = '#77C5D5';
            break;
        case 7:
            month = "Aug";
            currentMonthColor1 = '#8C4799';
            currentMonthColor2 = '#A15A95';
            break;
        case 8:
            month = "Sep";
            currentMonthColor1 = '#E07E3C';
            currentMonthColor2 = '#FF7F41';
            break;
        case 9:
            month = "Oct";
            currentMonthColor1 = '#6FA287';
            currentMonthColor2 = '#A9C47F';
            break;
        case 10:
            month = "Nov";
            currentMonthColor1 = '#00558C';
            currentMonthColor2 = '#0086BF';
            break;
        case 11:
            month = "Dec";
            currentMonthColor1 = '#E04E39';
            currentMonthColor2 = '#E56A54';
            break;

    }

    //Build first tile for the month
    html += '<div class="event-tile month" style="background-color: ' + currentMonthColor1 + '" aria-valuetext="Click here to see all events" title="Click here to see all events"><div id="month_' + today.getMonth() + '"><a href="/lists/events/calendar.aspx?CalendarDate=' + (today.getMonth() + 1) + '/1&CalendarPeriod=Month">' + month + '</a></div></div>';

    //Build tiles for the upcoming 5 days
    var newMonthExists = false;
    var daysToAdd = 0;

    for (d = 0; d < dayCount; d++) {
        
        if (d === 0) {
            html += '<div class="event-tile" style="background-color: ' + currentMonthColor2 + '" id="day_' + date + '">\
			    <div class="day">' + date + '</div>\
			    <small class="day-events"></small>\
		    </div>';
        } else {
            var newDate = new Date(today);
            newDate.setDate(today.getDate() + daysToAdd);

            switch (newDate.getMonth()) {
                case 0:
                    newMonth = "Jan";
                    newMonthColor1 = '#3EB1C8';
                    newMonthColor2 = '#77C5D5';
                    break;
                case 1:
                    newMonth = "Feb";
                    newMonthColor1 = '#8C4799';
                    newMonthColor2 = '#A15A95';
                    break;
                case 2:
                    newMonth = "Mar";
                    newMonthColor1 = '#E07E3C';
                    newMonthColor2 = '#FF7F41';
                    break;
                case 3:
                    newMonth = "Apr";
                    newMonthColor1 = '#6FA287';
                    newMonthColor2 = '#A9C47F';
                    break;
                case 4:
                    newMonth = "May";
                    newMonthColor1 = '#00558C';
                    newMonthColor2 = '#0086BF';
                    break;
                case 5:
                    newMonth = "Jun";
                    newMonthColor1 = '#E04E39';
                    newMonthColor2 = '#E56A54';
                    break;
                case 6:
                    newMonth = "Jul";
                    newMonthColor1 = '#3EB1C8';
                    newMonthColor2 = '#77C5D5';
                    break;
                case 7:
                    newMonth = "Aug";
                    newMonthColor1 = '#8C4799';
                    newMonthColor2 = '#A15A95';
                    break;
                case 8:
                    newMonth = "Sep";
                    newMonthColor1 = '#E07E3C';
                    newMonthColor2 = '#FF7F41';
                    break;
                case 9:
                    newMonth = "Oct";
                    newMonthColor1 = '#6FA287';
                    newMonthColor2 = '#A9C47F';
                    break;
                case 10:
                    newMonth = "Nov";
                    newMonthColor1 = '#00558C';
                    newMonthColor2 = '#0086BF';
                    break;
                case 11:
                    newMonth = "Dec";
                    newMonthColor1 = '#E04E39';
                    newMonthColor2 = '#E56A54';
                    break;
            }
            if (newDate.getMonth() != today.getMonth() && newMonthExists === false) {             
                
                html += '<div class="event-tile month-ahead month" style="background-color: ' + newMonthColor1 + '"><div id="month_' + newDate.getMonth() + '"><a href="/lists/events/calendar.aspx?CalendarDate=' + (newDate.getMonth() + 1) + '/1&CalendarPeriod=Month">' + newMonth + '</a></div></div>';
                newMonthExists = true;
                d++;
                
            }

            if (newMonthExists === true && d < 5) {

                html += '<div class="event-tile new-month" style="background-color: ' + newMonthColor2 + '" id="day_' + newDate.getDate() + '">\
			        <div class="day">' + newDate.getDate() + '</div>\
			        <small class="day-events"></small>\
		        </div>';


            } else {
                if (newMonthExists === true && d < 4 || newMonthExists === false) {
                    html += '<div class="event-tile" style="background-color: ' + newMonthColor2 + '" id="day_' + newDate.getDate() + '">\
			            <div class="day">' + newDate.getDate() + '</div>\
			            <small class="day-events"></small>\
		            </div>';
                }

            }

           
        }
        daysToAdd++;
    }

    html += '</div>';

    $('.events-container').html(html);

    for (i = 0; i < data.results.length; i++) {
        var startDate = new Date(data.results[i].EventDate);
        if ($('#month_' + startDate.getMonth()).length > 0 && $('#day_' + startDate.getUTCDate()).length > 0) {
            if (data.results[i].Title != 'Casual Day') {
                if (data.results[i].MaximumAttendees > 0) {
                    $('#day_' + startDate.getUTCDate() + ' small').append('<div class="event" data-eventid="' + data.results[i].Id + '"><a href="/Lists/Events/DispForm.aspx?ID=' + data.results[i].Id + '">' + data.results[i].Title + '</a><br><a class="register-button btn btn-xs btn-success" style="display: none;" data-eventid="' + data.results[i].Id + '" data-eventtitle="' + data.results[i].Title + '" data-max="' + data.results[i].MaximumAttendees + '" onclick="javascript:registerForEvent(this);"><span class="glyphicon glyphicon-plus"></span> Register Now</div><br>');
                } else {
                    $('#day_' + startDate.getUTCDate() + ' small').append('<div class="event" data-eventid="' + data.results[i].Id + '"><a href="/Lists/Events/DispForm.aspx?ID=' + data.results[i].Id + '">' + data.results[i].Title + '</a><br>');
                }
            } else {
                $('#day_' + startDate.getUTCDate()).append('<div class="event-icon"><span Title="It\'s a casual day">C</span></div>')
            }
        }
    }

    //$('.event').hover(function () { $(this).find('.register-button').show() }, function () { $(this).find('.register-button').hide() });
    $('.event').hover(function () { checkRegistration(this); }, function () { $(this).find('.register-button').hide(); });
}

function showModalDialog(title,url,link) {
    var options = {
        title: title,
        width: 600,
        height: 600,
        url: url,
        dialogReturnValueCallback: function (dialogResult) {
            SP.UI.ModalDialog.RefreshPage(dialogResult)
        }
    };

    SP.UI.ModalDialog.showModalDialog(options);

}

function editQuickLink(title, url, link) {
    var options = {
        title: title,
        width: 600,
        height: 600,
        url: url,
        dialogReturnValueCallback: function () {
            getQuickLinks();
        }
    };

    SP.UI.ModalDialog.showModalDialog(options);

}

function addToMyQuickLinks() {

    var link = document.location.pathname;

    showModalDialog("Add quick Link", url, link);
}

function quickLinksQuickAdd() {

    var url = document.location.pathname;
    var metadata = { 
        Title: $('title')[0].innerText,
        URL: 
                {
                    '__metadata': { 'type': 'SP.FieldUrlValue' },
                    'Description': $('title')[0].innerText,
                    'Url': url
                },
        IconURL: '/Icons/Default.png'
        
    }

    addListItem('Quick Links User', metadata, browseToItem)

}

function browseToItem(data) {

    
    //setTimeout(function () { showModalDialog('Pick an image for your quick link', '/Lists/QuickLinksUser/EditForm.aspx?ID=' + data.d.Id + '?IsDlg=1', '/'); }, 5000);
    addStatusMessage("Your quick link has been added! <a href='/'>Click here to go to the homepage and update the <strong>image</strong> for your link.</a>", true);
}

//Page Comments
function postComment(d) {
    

    var metadata = {
        Title: _spPageContextInfo.serverRequestPath,
        PageId: _spPageContextInfo.pageItemId.toString(), 
        ListId: _spPageContextInfo.pageListId.toString(),
        Comment: $('#comment').val()
    }

    if (metadata.Comment != "") {
        addListItem("Comments", metadata, getComments);
    } else {
        $('#commentMessage').show();
        setTimeout(function () { $('#commentMessage').hide(); }, 7000);
    }

}

function getComments() {

    $('#comment').val('');
    getListItems("Comments", "?$filter=((PageId eq '" + _spPageContextInfo.pageItemId.toString() + "') and (ListId eq '" + _spPageContextInfo.pageListId.toString() + "'))&$orderby=Created desc", showComments);
}

function getMoreComments() {

    $('#comment').val('');
    $('#comments table tr').each(function () { $(this).show(); });
    $('#seeAllComments').hide();
}

function showComments(data) {

    if (data.results.length > 0) {

        var html = '<table>';
        var i;
        

        for (i = 0; i < data.results.length; i++) {

            var created = new Date(data.results[i].Created);
            var author = getSiteUserById(data.results[i].AuthorId);
            var profileImg;
            var pageId = data.results[i].PageId;
            if (author.pictureUrl == null) {
                profileImg = '/_layouts/15/images/PersonPlaceholder.42x42x32.png';
            } else {
                profileImg = author.pictureUrl;
            }
            if (i < 5) {
                if (data.results[i].AuthorId.toString() === _spPageContextInfo.userId.toString()) {
                    html += '<tr class="comment" id="author_' + data.results[i].AuthorId + '">\
                        <td rowspan="2" class="comment-img" valign="top"><img width="50" src="' + profileImg + '" alt="" /></td>\
                        <td class="comment-title"><strong>' + author.title + '</strong><small>  ' + (created.getMonth() + 1) + '/' + created.getDate() + '/' + created.getFullYear() + '</small><span class="delete-action pull-right"> <a data-itemid="' + data.results[i].Id + '" onclick="deleteComment(this);"><img src="/_layouts/15/images/EMMDeleteTerm.png" alt="Delete this comment" Title="Delete this comment" /></a></span></td>\
                    </tr>\
                    <tr>\
                        <td valign="top" class="comment-msg">' + data.results[i].Comment + '</td>\
                    </tr>';
                } else {
                    html += '<tr class="comment" id="author_' + data.results[i].AuthorId + '">\
                        <td rowspan="2" class="comment-img" valign="top"><img width="50" src="' + profileImg + '" alt="" /></td>\
                        <td class="comment-title"><strong>' + author.title + '</strong><small>  ' + (created.getMonth() + 1) + '/' + created.getDate() + '/' + created.getFullYear() + '</small></td>\
                    </tr>\
                    <tr>\
                        <td valign="top" class="comment-msg">' + data.results[i].Comment + '</td>\
                    </tr>';
                }
            } else {
                if (data.results[i].AuthorId.toString() === _spPageContextInfo.userId.toString()) {
                    html += '<tr class="comment" id="author_' + data.results[i].AuthorId + '" style="display:none;">\
                        <td rowspan="2" class="comment-img" valign="top"><img width="50" src="' + profileImg + '" alt="" /></td>\
                        <td class="comment-title"><strong>' + author.title + '</strong><small>  ' + (created.getMonth() + 1) + '/' + created.getDate() + '/' + created.getFullYear() + '</small><span class="delete-action pull-right"> <a data-itemid="' + data.results[i].Id + '" onclick="deleteComment(this);"><img src="/_layouts/15/images/EMMDeleteTerm.png" alt="Delete this comment" Title="Delete this comment" /></a></span></td>\
                    </tr>\
                    <tr style="display:none;">\
                        <td valign="top" class="comment-msg">' + data.results[i].Comment + '</td>\
                    </tr>';
                } else {
                    html += '<tr class="comment" id="author_' + data.results[i].AuthorId + '" style="display:none;">\
                        <td rowspan="2" class="comment-img" valign="top"><img width="50" src="' + profileImg + '" alt="" /></td>\
                        <td class="comment-title"><strong>' + author.title + '</strong><small>  ' + (created.getMonth() + 1) + '/' + created.getDate() + '/' + created.getFullYear() + '</small></td>\
                    </tr>\
                    <tr style="display:none;">\
                        <td valign="top" class="comment-msg">' + data.results[i].Comment + '</td>\
                    </tr>';
                }
            }

        }
        
        html += '</table>';
        $('#comments').html(html);
        if (data.results.length > 5) {
            $('#seeAllComments').show();
        } else {
            $('#seeAllComments').hide();
        }
        $('#comments-container h4:first').show();
        $('#comments').show();
    } else {
        $('#comments-container h4:first').hide();
        $('#comments').html('');
        $('#comments').hide();
    }

}

function deleteComment(e) {
    
    var id = $(e).data("itemid");

    // getting our item to delete, then executing a delete once it's been returned
    getListItem("", "Comments", id, function (data) {
            $.ajax({
                url: data.d.__metadata.uri,
                type: "POST",
                headers: {
                    "Accept": "application/json;odata=verbose",
                    "X-Http-Method": "DELETE",
                    "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                    "If-Match": data.d.__metadata.etag
                },
                success: function (data) {            
                    getComments();
                },
                error: function (data) {
                    addStatusMessage("Error: " + data.responseText, false);
                }
            });
        });

}

function getSiteUserById(id) {

    var user = {};

    //Get Site user  
    $.ajax({
        url: "/_api/web/siteusers/getbyid(" + id + ")", //Use OData operators such as $Select=Title eq 'My Title'
        method: "GET",
        async: false,
        headers: { "Accept": "application/json; odata=verbose" },
        success: function (data) {

            //console.log(data);
            user.title = data.d.Title;
            

            $.ajax({
                url: "/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v='" + data.d.LoginName.split("|")[1] + "'", //Use OData operators such as $Select=Title eq 'My Title'
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (userData) {

                    //console.log(data);
                    user.pictureUrl = userData.d.PictureUrl;


                },
                error: function (userData) {
                    addStatusMessage("Error: " + userData.responseText, false);
                }
            });

            
        },
        error: function (data) {
            addStatusMessage("Error: " + data.responseText, false);
        }
    });

    return user;
}


//function likePage(like) {

//    var context = new SP.ClientContext;

//    Microsoft.Office.Server.ReputationModel.Reputation.setLike(context,_spPageContextInfo.pageListId , _spPageContextInfo.pageItemId, like);

//    context.executeQueryAsync(
//        function () {
//            // Do something if successful
//            getPageLikes();
//        }, function (sender, args) {
//            // Do something if error
//            addStatusMessage("Error: " + args, false);
//        });
    
//}

function getUserLikedPage() {
    var context = new SP.ClientContext(_spPageContextInfo.webServerRelativeUrl);
    var list = context.get_web().get_lists().getById(_spPageContextInfo.pageListId);
    var item = list.getItemById(_spPageContextInfo.pageItemId);

    context.load(item, "LikedBy", "ID", "LikesCount");
    context.executeQueryAsync(Function.createDelegate(this, function (success) {
        // Check if the user id of the current users is in the collection LikedBy. 
        var $v_0 = item.get_item('LikedBy');
        if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
            for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                var $v_3 = $v_0[$v_1];
                if ($v_3.$1E_1 === _spPageContextInfo.userId) {

                    //User has already liked this page. Change the style and change click event to remove like.
                    $('#likeButton').addClass('liked');
                    $('#likeButton').attr('title', 'You have already liked this page. Click again to remove.');

                }
            }
        }

        getPageLikes();
        
    }),
        Function.createDelegate(this, function (sender, args) {
            //Custom error handling if needed 
        }));
}

var likepage = {
    //Likes the current page. 
    LikePage: function () {
        likepage.getUserLikedPage(function (likedPage, likeCount) {

            var aContextObject = new SP.ClientContext();
            EnsureScriptFunc('reputation.js', 'Microsoft.Office.Server.ReputationModel.Reputation', function () {
                Microsoft.Office.Server.ReputationModel.
                    Reputation.setLike(aContextObject,
                        _spPageContextInfo.pageListId.substring(1, 37),
                        _spPageContextInfo.pageItemId, !likedPage);

                aContextObject.executeQueryAsync(
                    function () {
                        var elements = document.getElementsByClassName('badge');
                        if (likedPage) {
                            likeCount--;
                            $('#likeButton').removeClass('liked');
                            $('#likeButton').attr('Title', 'Click here to like this page.');
                        } else {
                            likeCount++;
                            $('#likeButton').addClass('liked');
                            $('#likeButton').attr('Title', 'You have already liked this page. Click again to unlike the page.');
                        }
                        for (var i = 0; i < elements.length; i++) {
                            elements[i].innerHTML = likeCount;
                        }


                    }, function (sender, args) {
                        // Custom error handling if needed

                    });
            });
        });

    },
    // Checks if the user already liked the page, and returns the number of likes. 
    getUserLikedPage: function (cb) {
        var context = new SP.ClientContext(_spPageContextInfo.webServerRelativeUrl);
        var list = context.get_web().get_lists().getById(_spPageContextInfo.pageListId);
        var item = list.getItemById(_spPageContextInfo.pageItemId);

        context.load(item, "LikedBy", "ID", "LikesCount");
        context.executeQueryAsync(Function.createDelegate(this, function (success) {
            // Check if the user id of the current users is in the collection LikedBy. 
            var $v_0 = item.get_item('LikedBy');
            if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
                for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                    var $v_3 = $v_0[$v_1];
                    if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                        cb(true, item.get_item('LikesCount'));
                        $('#likeButton').addClass('liked');
                    } else {
                        $('#likeButton').removeClass('liked');
                    }
                }
            }
            cb(false, item.get_item('LikesCount'));
            $('#likeButton').removeClass('liked');
        }),
            Function.createDelegate(this, function (sender, args) {
                //Custom error handling if needed 
            }));
    },
    checkUserLikedPage: function () {
        var context = new SP.ClientContext(_spPageContextInfo.webServerRelativeUrl);
        var list = context.get_web().get_lists().getById(_spPageContextInfo.pageListId);
        var item = list.getItemById(_spPageContextInfo.pageItemId);

        context.load(item, "LikedBy", "ID", "LikesCount");
        context.executeQueryAsync(Function.createDelegate(this, function (success) {
            // Check if the user id of the current users is in the collection LikedBy. 
            var $v_0 = item.get_item('LikedBy');
            if (!SP.ScriptHelpers.isNullOrUndefined($v_0)) {
                for (var $v_1 = 0, $v_2 = $v_0.length; $v_1 < $v_2; $v_1++) {
                    var $v_3 = $v_0[$v_1];
                    if ($v_3.$1E_1 === _spPageContextInfo.userId) {
                        $('#likeButton').addClass('liked');
                        $('#likeButton').attr('Title', 'You have already liked this page. Click again to unlike the page.');
                    } else {
                        $('#likeButton').removeClass('liked');
                        $('#likeButton').attr('Title', 'Click here to like this page.');
                    }
                }
            }
            getPageLikes();
        }),
        Function.createDelegate(this, function (sender, args) {
            //Custom error handling if needed 
        }));
    },
    initialize: function () {
        var elements = document.getElementsByClassName('badge');
        likepage.getUserLikedPage(function (likedPage, likesCount) {
            for (var i = 0; i < elements.length; i++) {
                elements[i].innerHTML = likesCount;
            }
        });
    }
};
//_spBodyOnLoadFunctionNames.push("likepage.initialize");



function getPageLikes() {

    
    getListItem('', "Pages", _spPageContextInfo.pageItemId, function (data) {

        if (data.d.LikesCount > 0) {

            $('.page-actions .badge').each(function() {

                $(this).text(data.d.LikesCount);

            });
                
        }

        //getUserLikedPage();

    });
    
}

//General Functions
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Notification and Messaging Functions

function addStatusMessage(msg, status) {

    var statusId = SP.UI.Status.addStatus(msg);

    if (status == true) {
        SP.UI.Status.setStatusPriColor(statusId, 'green');
        setTimeout(function () { removeStatus(statusId) }, 7000);
    } else {
        SP.UI.Status.setStatusPriColor(statusId, 'red');
        setTimeout(function () { removeStatus(statusId) }, 7000);
    }

}


function RemoveAllStatus(id) {
    SP.UI.Status.removeStatus(id);

}

function getEvent() {

    var eventId = getParameterByName('ID');

    getListItem('', 'Events', eventId, getEventRegistrations);

}

function getEventRegistrations(eventData) {

    
    if (eventData.d.MaximumAttendees > 0) {
        $.ajax({
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('Registrations')/items?$Filter=EventId eq '" + eventData.d.Id + "'", 
            method: "GET",
            headers: { "Accept": "application/json; odata=verbose" },
            success: function (data) {
                var percentage = ((data.d.results.length / eventData.d.MaximumAttendees) * 100);
                if (percentage === 0) {
                    var html = '<a class="btn btn-success" style="display: none;" data-eventid="' + eventData.d.Id + '" data-eventtitle="' + eventData.d.Title + '" data-max="' + eventData.d.MaximumAttendees + '" id="register" onclick="javascript:registerForEvent(this);">Register Now</a> \
                                <h4>Be the first to register</h4>';
                    $('.registration-container').html(html);
                }
                if (percentage != 100 && percentage > 0) {
                    var html = '<a class="btn btn-success" style="display: none;" data-eventid="' + eventData.d.Id + '" data-eventtitle="' + eventData.d.Title + '" data-max="' + eventData.d.MaximumAttendees + '" id="register" onclick="javascript:registerForEvent(this);">Register Now</a> <a class="btn btn-danger" style="display: none;" data-eventid="' + eventData.d.Id + '" data-eventtitle="' + eventData.d.Title + '" id="unregister" onclick="javascript:unRegisterForEvent(this);">Unregister</a>\
                                <h4>Current Registrations</h4>\
                                <div class="progress">\
                                  <div class="progress-bar" role="progressbar" aria-valuenow="' + Math.ceil(percentage) + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + Math.ceil(percentage) + '%;">\
                                    ' + Math.ceil(percentage) + '%\
                                  </div>\
                                </div>';
                    $('.registration-container').html(html);
                } 
                if (percentage === 100) {
                    $('.registration-container').html('<h4>This event is full</h4><a style="display: none; cursor: pointer;" data-eventid="' + eventData.d.Id + '" data-eventtitle="' + eventData.d.Title + '" id="unregister" onclick="javascript:unRegisterForEvent(this);">Unregister</a><br>');
                }
                checkMyRegistration(eventData.d.Id);
            },
            error: function (data) {
                addStatusMessage("Error: " + data.responseText, false);
            }
        });
    } else {
        $('.registration-container').html('<h4>Registration not open for this event.</h4>');
    }

}

function checkMyRegistration(id) {
    getListItems("Registrations", "?$Filter=((EventId eq '" + id + "') and (Author eq '" + _spPageContextInfo.userId + "'))", updateRegistrationButtons);
}

function updateRegistrationButtons(data) {

    if (data.results.length > 0) {
        $('#register').hide();
        $('#unregister').attr('data-registrationid', data.results[0].Id);
        $('.registration-container').append("You are registered for this event<br> <a href='/_vti_bin/owssvr.dll?CS=109?Cmd=Display&CacheControl=1&List=" + _spPageContextInfo.pageListId.substring(1, 37) + "&ID=" + $('#register').data('eventid') + "&Using=Lists%2fEvents/event.ics '>Download the iCal file here.</a><br><a href='/Lists/Registrations/AllItems.aspx?FilterField1=EventId&FilterValue1=" + $('#register').data('eventid') + "'>See all registrations</a>");
        $('#unregister').show();
    } else {
        $('#register').show();
        $('#unregister').hide();
    }

}

function registerForEvent(e) {


    //Get all registrations
    var registrations = getRegistrationsForEvent($(e).data('eventid').toString());

    //Check to see if user has already registered
    var myRegistrations = $.grep(registrations.results, function (item) { return item.Author == _spPageContextInfo.userId });

    if (registrations.results.length < $(e).data('max')) {
        if (myRegistrations.length === 0) {
            var location = getMyPropertiesAsync();

            var office = 'DM';
            if (location.indexOf('DM') > 0) { office = "DM"; }
            if (location.indexOf('SF') > 0) { office = "SF"; }
            if (location.indexOf('CR') > 0) { office = "CR"; }
            var metadata = { EventId: $(e).data('eventid').toString(), Title: $(e).data('eventtitle'), Location: office }

            if (_spPageContextInfo.serverRequestPath.toLowerCase() == "/pages/default.aspx") {
                addListItem("Registrations", metadata, function () { window.location.href = '/Lists/Events/DispForm.aspx?ID=' + $(e).data('eventid') });
            } else {
                addListItem("Registrations", metadata, updateEventStatus);
            }
        } else {
            alert("You have already registered for this event");
        }
    }



}

function updateEventStatus() {

    addStatusMessage("You have successfully registered for this event! <a href='/_vti_bin/owssvr.dll?CS=109?Cmd=Display&CacheControl=1&List=" + _spPageContextInfo.pageListId.substring(1, 37) + "&ID=" + $('#register').data('eventid') + "&Using=Lists%2fEvents/event.ics '>Download the iCal file here.</a>", true)
    setTimeout(function () { $('.registration-container').append("<a href='/_vti_bin/owssvr.dll?CS=109?Cmd=Display&CacheControl=1&List=" + _spPageContextInfo.pageListId.substring(1, 37) + "&ID=" + $('#register').data('eventid') + "&Using=Lists%2fEvents/event.ics '>Download the iCal file here.</a>"); }, 7000);   
    getEvent();
}

function unRegisterForEvent(e) {

    var id = $(e).data('registrationid');

    // getting our item to delete, then executing a delete once it's been returned
    getListItem("", "Registrations", id, function (data) {
        $.ajax({
            url: data.d.__metadata.uri,
            type: "POST",
            headers: {
                "Accept": "application/json;odata=verbose",
                "X-Http-Method": "DELETE",
                "X-RequestDigest": $("#__REQUESTDIGEST").val(),
                "If-Match": data.d.__metadata.etag
            },
            success: function (data) {
                getEvent();
                addStatusMessage("You have successfully unregistered for this event. Remember to remove this event from you Outlook calendar if you imported an iCal file.", true)
            },
            error: function (data) {
                addStatusMessage("Error: " + data.responseText, false);
            }
        });
    });

}

function showNewsBannerPreview() {

    var img = $(".edit-mode-panel [id$='ImageFieldDisplay'] img").attr('src');
    var content = $(".edit-mode-panel [id$='RichHtmlField_displayContent']").html();
    var html = '<div class="banner-image">\
                    <a>\
                        <img src="' + img + '" />\
                    </a>\
                </div>\
                <div class="slide-content">\
                    ' + content + '\
                </div>';

    $('.banner-preview').html(html);
    $('.banner-preview').show();


}