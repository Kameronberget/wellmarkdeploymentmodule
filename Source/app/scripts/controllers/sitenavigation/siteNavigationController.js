var globalCtr = 0;

(function () {
    angular.module("WellmarkNavigation")
        .controller("siteNavigationController", ["$scope", "spDataSvc", "$attrs", "$q",
            function ($scope, spDataSvc, attrs, $q) {
    
                $scope.columns = !attrs.columns ? "4" : attrs.columns;
                $scope.qry = !attrs.view ? "" : "?$filter=View eq '" + attrs.view + "'";
                $scope.url = !attrs.navlist? "" : attrs.navlist;
                $scope.list = "Navigation";
                

                spDataSvc.getListItems($scope.url, $scope.list, $scope.qry)
                    .then(function (response) {
                        for (var i = 0; i < response.d.results.length; i++) {
                           $scope.navLinks = response.d.results;

                        }
                        $scope.navsettings = {
                            "View": attrs.view,
                            "Url": attrs.navlist,
                            "NavList": "Navigation",
                            "Columns": attrs.columns,
                            "Query": $scope.qry,
                            "ItemCount": response.d.results.length
                        }
                        console.log(JSON.stringify($scope.navsettings));
                    });

                $scope.testAccess1 = function (url) {
                    
                    var show;
                    
                    spDataSvc.getSite(url)
                    .then(function (response) {

                        if (response.indexOf("AccessDenied") > 0) {
                            show = false;
                        } else {
                            show = true;
                        }

                    }, function(reason) {
                        show = false;
                        console.log("Hidden: " + url);
                    });
                    console.log(show);
                    return show;
                }; 

                $scope.testAccess1 = function (url) {
                  // Overriding the URL for testing purposes
                  //url = 'https://www.googleapis.com/discovery/v1/apis?fields=';
                  
                  // Prevent infinite loop from hosing up Plunker
                  if (globalCtr++ > 100) return false;
                 
                  var accessDeferred = $q.defer();
                  
                  spDataSvc.getSite(url)
                  .then(function (response) {
                      var show = (typeof response === 'string' && response.indexOf("AccessDenied") === -1);
                      
                      console.log(response);
                      console.log('Yay! I got here!');
                      
                      accessDeferred.resolve(show);

                  }, function(reason) {
                      var show = false;
                      
                      console.log(reason);
                      console.log('Boo! Some error occurred.');
                      
                      accessDeferred.resolve(show);
                  });

                  return accessDeferred.promise;
                };  
                
            }])
        .directive('sitenavigation', function () {

            var view = "/navigation/navigation.html";
            var templateUrl = !window._spPageContextInfo ? "../../templates" : _spPageContextInfo.siteServerRelativeUrl + "/style library/byron/templates";
            return {
                restrict: 'E',
                templateUrl: templateUrl + view
            };
        });
})();