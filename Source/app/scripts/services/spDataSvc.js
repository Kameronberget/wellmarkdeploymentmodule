// This should become spListSvc.js getListItems, get

(function() {
    angular.module("WellmarkNavigation")
        .factory("spDataSvc", ["baseSvc", function(baseSvc) {
            
            var listEndPoint = '/_api/web/lists';
            var webEndPoint = "/_api/web/doesuserhavepermissions(@v)?@v={'High':'1073741826', 'Low':'1073741826'}";
            
            var getListItems = function(url, listName, qry) {
                var endPoint = !window._spPageContextInfo ? "api/" + listName + "/" + listName + ".json" : url + listEndPoint + "/GetByTitle('" + listName + "')/items" + qry;
                return baseSvc.getRequest(endPoint);
            };

            var getSite = function(url) {
                return baseSvc.getRequest(url);
            };

            var checkSiteAccess = function (url) {
                var endPoint = !window._spPageContextInfo ? "api/web/web.json.json" : url + webEndPoint;
                return baseSvc.getRequest(endPoint);
            }


            return {
                getListItems: getListItems,
                getSite: getSite,
                checkSiteAccess: checkSiteAccess
            };
        }]);
})();