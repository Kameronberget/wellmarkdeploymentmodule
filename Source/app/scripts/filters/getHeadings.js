app.filter('getHeadings', function () {
    return function (items, parent) {
        var headings = [];
        angular.forEach(items, function (item) {
            
            if (headings.indexOf(item.Heading) == -1) {
                headings.push(item.Heading)
            }
            
        })
        return headings;
    };
});