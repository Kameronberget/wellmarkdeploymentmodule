app.filter('getChildNavs', function () {
    return function (items, parent) {
        var childItems = [];
        angular.forEach(items, function (item) {

            if (item.ParentId == parent.Id) {
                childItems.push(item)
            }
            
        })
        return childItems;
    };
});