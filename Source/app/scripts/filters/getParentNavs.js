app.filter('getParentNavs', function () {
    return function (items, heading) {
        var parentItems = [];
        angular.forEach(items, function (item) {
            
            // console.log("Finding links with heading of: " + item.Heading);
            if (item.ParentId == null && item.Heading == heading) {
                parentItems.push(item)
            }
            
        })
        return parentItems;
    };
});