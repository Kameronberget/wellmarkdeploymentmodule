var gulp = require('gulp');
var order = require("gulp-order");
var concat = require('gulp-concat');
var minifier = require('gulp-uglify');


// Inside Wellmark Publishing Deployment Tasks
gulp.task('bundle:js:pub', function() {
  gulp.src([
      "app/scripts/**/**/*.js", 
      "!app/scripts/vendor/**/*.js",  
      "!app/scripts/custom/**/*.js"
    ]) 
    .pipe(concat("insidewellmark.built.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});

gulp.task('bundle:custom:pub', function() {
  gulp.src(["app/scripts/custom/insidewellmark/*.js"]) 
    .pipe(concat("insidewellmark.custom.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});

gulp.task('bundle:css:pub', function() {
  gulp.src([
      "app/styles/common/bootstrap3.min.css",
      "app/styles/common/bootstrap2-custom.css",
      "app/styles/common/sp-responsive.css",
      "app/styles/common/bootstrap3-custom.min.css",
      "app/styles/insidewellmark/wellmark.css",
      "app/styles/insidewellmark/megamenu.css",
      "app/styles/insidewellmark/touchcarousel.css",
      "app/styles/insidewellmark/grey-blue-skin.css"
    ]) 
    .pipe(concat("insidewellmark.main.min.css"))
    .pipe(gulp.dest("../Dist/css"));
});

gulp.task('bundle:images:pub', function() {
  gulp.src('app/images/*.*').pipe(gulp.dest('../Dist/images'))
});

gulp.task('bundle:vendor:pub', function() {
  gulp.src([
      "app/scripts/vendor/common/jquery-1.10.2.js",
      "app/scripts/vendor/common/bootstrap3.min.js",
      "app/scripts/vendor/common/bootstrap3-custom.js",
      "node_modules/angular/angular.min.js",
      "app/scripts/vendor/insidewellmark/*.js"
    ]) 
    .pipe(concat("insidewellmark.vendor.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});


gulp.task('copy:fonts:pub', function() {
  gulp.src('app/fonts/*.*').pipe(gulp.dest('../Dist/fonts'));
});

gulp.task('copy:masterpages:pub', function() {
  gulp.src(['app/masterpages/insidewellmark/*.master']).pipe(gulp.dest('../Dist/masterpages'));
});

gulp.task('copy:pagelayouts:pub', function() {
  gulp.src('app/pagelayouts/insidewellmark/*.aspx').pipe(gulp.dest('../Dist/pagelayouts'));
});

// Inside Wellmark Collaboration Deployment Tasks
gulp.task('bundle:js:collab', function() {
  gulp.src([
      "app/scripts/**/**/*.js", 
      "!app/scripts/vendor/**/*.js",
      "!app/scripts/custom/**/*.js"
    ]) 
    .pipe(concat("wellnetsp.built.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});

gulp.task('copy:html:collab', function() {
  gulp.src(["app/templates/**/*.html"]) 
    .pipe(concat("wellnetsp.built.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});

gulp.task('copy:images:collab', function() {
  gulp.src('app/images/*.*').pipe(gulp.dest('../Dist/images'))
});

gulp.task('bundle:custom:collab', function() {
  gulp.src(["app/scripts/custom/wellnetsp/*.js"]) 
    .pipe(concat("wellnetsp.custom.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});

gulp.task('bundle:css:collab', function() {
  gulp.src(["app/styles/wellnetsp/*.css", "app/styles/common/*.css"])
    .pipe(concat("wellnetsp.main.min.css"))
    .pipe(gulp.dest("../Dist/css"));
});


gulp.task('bundle:vendor:collab', function() {
  gulp.src([
      "app/scripts/vendor/common/jquery-1.10.2.js",
      "app/scripts/vendor/common/bootstrap3.min.js",
      "node_modules/angular/angular.min.js",
      "app/scripts/vendor/wellnetsp/*.js"
    ]) 
    .pipe(concat("wellnetsp.vendor.min.js"))
    .pipe(gulp.dest("../Dist/js"));
});


gulp.task('copy:fonts:collab', function() {
  gulp.src('app/fonts/*.*').pipe(gulp.dest('../Dist/fonts'));
});


gulp.task('copy:masterpages:collab', function() {
  gulp.src('app/masterpages/wellnetsp/*.master').pipe(gulp.dest('../Dist/masterpages'));
});

gulp.task('copy:pagelayouts:collab', function() {
  gulp.src('app/pagelayouts/wellnetsp/*.aspx').pipe(gulp.dest('../Dist/pagelayouts'));
});


//gulp.task('default', ['bundle:js', 'bundle:vendor', 'copy:html','bundle:css', 'copy:fonts', 'copy:masterpages', 'copy:pagelayouts']);
gulp.task('insidewellmark', ['bundle:js:pub', 'bundle:vendor:pub', 'bundle:custom:pub', 'bundle:css:pub', 'copy:fonts:pub', 'copy:masterpages:pub', 'copy:pagelayouts:pub','bundle:images:pub']);
gulp.task('wellnetsp', ['bundle:js:collab', 'bundle:custom:collab', 'bundle:vendor:collab', 'copy:html:collab','bundle:css:collab', 'copy:fonts:collab', 'copy:masterpages:collab', 'copy:pagelayouts:collab','copy:images:collab']);
