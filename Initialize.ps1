﻿$Global:settings = New-Object -TypeName PSObject -Property @{
    Path = $PSScriptRoot;
    BrandingXml = [xml](Get-Content ($PSScriptRoot.TrimEnd("\") + "\Configurations\Settings.xml"));
    LogPath = $PSScriptRoot.TrimEnd("\") + "\Logs"
}

Write-ToScreen -Message "Successfully loaded Wellmark Deployment Module" -Type OK