﻿function Write-ToScreen {
    Param (
		[Parameter(Mandatory=$True,Position=0)]
		[string]$Message,
		
        [Parameter(Mandatory=$True,Position=1)]
		[ValidateSet("OK","Warning","Error", "Info", "Verbose")] 
		[string]$Type
    )

    #Ensure event log is setup
    New-EventLog -LogName Application -Source "ByronDeploy" -ErrorAction SilentlyContinue
	#Write-ToScreen -Message "Windows EventLog for ByronDeploy created..." -Type OK
    

	# Write the message to the screen
    switch ($Type) {
		"Ok" {Write-Host -BackgroundColor Green -ForegroundColor Black  "    OK    " -NoNewline; $EntryType = "Information"}
		"Warning" {Write-Host -BackgroundColor Yellow -ForegroundColor Black "  Warning " -NoNewline;  $EntryType = "Warning"}
		"Error" {Write-Host -ForegroundColor Yellow -BackgroundColor Red  "   Error  " -NoNewline;  $EntryType = "Error"}
		"Info" {Write-Host -BackgroundColor Cyan -ForegroundColor Blue "   Info   " -NoNewline;  $EntryType = "Information"}
		"Verbose" {Write-Host -BackgroundColor Gray -ForegroundColor Black "  Verbose " -NoNewline;  $EntryType = "Information"}
	}

	$now = (Get-Date -Format "yyyy-MM-dd HH:mm:ss")
	
    $logtime = (Get-Date -Format "ddMMyyyy")
    $originalMessage = $Message
    $Message = (" " + $now + " | " + $Message)
    Write-Host $Message    
	#"[$Type] $message" | Out-File "$($Global:settings.LogPath)\Log_$logtime.log" -Append
    Write-EventLog -LogName Application -Source "ByronDeploy" -EntryType $EntryType -EventId 365 -Message $originalMessage
}