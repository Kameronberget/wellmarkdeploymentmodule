﻿function Publish-WellmarkAssets {
    [CmdletBinding()]
    param (

        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='You can provide a site that you want to target. If left blank, all sites in the branding configuration XML will be targeted.')]
	    [string]$Site,

        [Parameter(Mandatory=$False, Position=2, ValueFromPipeline=$false, HelpMessage='Which branding set do you want?')]
	    [ValidateSet('InsideWellmark','WellnetSP')]
        $webApplication,

        [Parameter(Mandatory=$false, Position=3, ValueFromPipeline=$false, HelpMessage='This switch triggers the deployment of necessary branding elements such as Stylesheets, master pages, page layouts, images, etc.')]
	    [switch]$DeployBranding,

		[Parameter(Mandatory=$false, Position=4, ValueFromPipeline=$false, HelpMessage='Allows you to only process a specific type of artifact in the site. Notice that this might result in a non-working site, as some of the handlers require other artifacts in place if they are not part of whatyour extracting.')]
        [ValidateSet('css','js','images','htmlFiles','fonts', 'masterpages', 'pagelayouts')]
        $Handlers,

        [Parameter(Mandatory=$false, Position=5, ValueFromPipeline=$false, HelpMessage='This switch triggers a gulp compile task to recompile all code prior to deploy.')]
	    [switch]$Compile,

        [Parameter(Mandatory=$False, Position=6, ValueFromPipeline=$false, HelpMessage='This switch triggers a download and install of the necessary PNP components.')]
        [switch]$InstallPNP
    )
    


    #PNP PowerShell
    if($InstallPNP) {
        if ((Get-Command | ?{$_.ModuleName -eq "OfficeDevPnP.PowerShell.V15.Commands"}).Count -eq 0) {
            Write-ToScreen -Message "Downloading PNP v15 (On premises) cmdlets now." -Type Verbose
            try {
                Read-Host "Ready to download the PNP engine now. Press any key to continue."
                Start-BitsTransfer -Source "https://github.com/OfficeDev/PnP-PowerShell/raw/master/Binaries/SharePointPnPPowerShell2013.msi" -Destination ($Global:Settings.Path + "\Downloads")
            } catch {
                Write-ToScreen -Message "Unable to download the PNP binaries. Please verify your internet connection." -Type Error
                break;
            }
            Write-ToScreen -Message "Finished downloading PNP v15 cmdlets." -Type Verbose
            Write-ToScreen -Message "Installing PNP v15 cmdlets." -Type Info
            try {
                Read-Host "PNP Engine downloaded. Press any key to install."
                Start-Process -FilePath $($Global:Settings.Path.TrimEnd("\") + "\" + "Downloads\SharePointPnPPowerShell2013.msi") -ArgumentList "/passive" -Wait
                Write-ToScreen "The PNP v15 cmdlet package was installed successfully. Please rerun the script." -Type 
            } catch {
                Write-ToScreen "There was an error trying to install the PNP v15 cmdlets. Rerun or access $($Global:Settings.Path)\Downloads\SharePointPnPPowerShell2013.msi to run the installer" -Type Error
            }
            break;
        } else {
            throw "PNP PowerShell already installed. Please remove the -InstallPNP switch and try again"
        }
        
    } else {
        if ((Get-Command | ?{$_.ModuleName -match "OfficeDevPnP"}).Count -le 0) {
            throw "The PNP PowerShell module is not installed please rerun the script with the -InstallPNP switch to download and install the module."
        }
    }

    

    if ([string]::IsNullOrEmpty($Site)) {
        
        Write-ToScreen -Message "You must enter a site URL to target" -Type Error
		
    } else {
        $connected = $false
        
		#Connect Site
        try {
            Write-ToScreen -Message "Connecting to $site" -Type Info
            Connect-SPOnline -Url $site -Credentials $Credentials
            Write-ToScreen -Message "Connected!" -Type OK
            $connected = $true
        } catch {
            Write-ToScreen -Message "Unable to connect to $site : $($Error[0]). PLease verify the URL and try again." -Type Error
            throw "Unable to connect to $site : $($Error[1]). PLease verify the URL and try again."
        }

        #Upload branding files
        if ($DeployBranding) {

            if ($compile) {
                if ($webApplication) {
                    cd ($Global:Settings.Path + "\Source")
                    gulp $webApplication.toLower() #Run tasks for web app
                    sleep 5
                } else {
                    cd ($Global:Settings.Path + "\Source")
                     @("InsideWellmark","WellnetSP") | %{ gulp $_.toLower()} #Run all gulp tasks
                    sleep 5
                }
            }

            if ($webApplication) {
                Deploy-Branding -webApplication $webApplication
            } else {
                @("InsideWellmark","WellnetSP") | %{Deploy-Branding -webApplication $_}
            }
        }
        
        Disconnect-SPOnline
    }

    

    
}

function Deploy-Branding {
	[CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the branding set you would like.')]
	    [string]$webApplication
    )
	
    if (!$handlers) {
        if ($webApplication) {
            Upload-Files -fileType "css" -webApplication $webApplication
            Upload-Files -fileType "js" -webApplication $webApplication
            Upload-Files -fileType "images" -webApplication $webApplication
            Upload-Files -fileType "fonts" -webApplication $webApplication
            Upload-Files -fileType "htmlFiles" -webApplication $webApplication
            Upload-MasterPages -webApplication $webApplication
            Upload-PageLayouts -webApplication $webApplication
        } else {
             @("InsideWellmark","WellnetSP") | %{ 
                Upload-Files -fileType "css" -webApplication $_
                Upload-Files -fileType "js" -webApplication $_
                Upload-Files -fileType "images" -webApplication $_
                Upload-Files -fileType "fonts" -webApplication $_
                Upload-Files -fileType "htmlFiles" -webApplication $_
                Upload-MasterPages -webApplication $_
                Upload-PageLayouts -webApplication $_
             }
        }
	} else {
        
        $a = $handlers.split(",")
		
        foreach ($f in $a) {
            
            if ($f.toLower() -eq "masterpages" -or $f.toLower() -eq "pagelayouts") {
                switch ($f) {
                    "masterpages" {
                        if ($webApplication) {
                            Upload-MasterPages -webApplication $webApplication
                        } else {
                            @("InsideWellmark","WellnetSP") | %{ Upload-Files -fileType $f -webApplication $_ }
                        }
                    }
                    "pagelayouts" {
                        if ($webApplication) {
                            Upload-PageLayouts -webApplication $webApplication
                        } else {
                            @("InsideWellmark","WellnetSP") | %{ Upload-Files -fileType $f -webApplication $_ }
                        }
                    }
                }
            } else {
                if ($webApplication) {
                    Upload-Files -fileType $f -webApplication $webApplication
                } else {
                    @("InsideWellmark","WellnetSP") | %{ Upload-Files -fileType $f -webApplication $_ }
                }
            }
        }		
	}    
}

function Upload-Files {
    
	[CmdletBinding()]    
    param (
        [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the types of files you wish to upload.')]
	    [string]$fileType,

        [Parameter(Mandatory=$true, Position=2, ValueFromPipeline=$false, HelpMessage='Provide the branding set you would like.')]
	    [string]$webApplication
    )
	
	Write-ToScreen -Message "Uploading $fileType files to $((Get-SPOContext).Url)" -Type Info

    foreach ($f in $Global:settings.BrandingXml.configuration.branding.$webApplication.$fileType.files.file) {
        $filePath = ($Global:settings.Path.TrimEnd("\") + "\Dist\" + $f.name.TrimStart("\"))
        $folder = $f.path + "/" + $f.folder + "/" + ($f.name.Substring(0, $f.name.LastIndexOf("\")))
        $serverRealtiveUrl = "/" + $f.path + "/" + $f.folder + "/" + $f.name.Replace("\","/")
        if ((Test-Path $filePath) -and $fileType.ToLower()) { # -ne "css" -and $fileType.ToLower() -ne "js"
            Write-ToScreen -Message "Uploading $(($f.name.Split("\") | select -Last 1)) to '$serverRealtiveUrl'" -Type Ok
            Set-SPOFileCheckedOut -Url $serverRealtiveUrl -ErrorAction SilentlyContinue       
            Upload-File -filePath $filePath -folder $folder.Replace("\","/")
        } else {
            $file = Get-Item $filePath
            if ($file) {
                $fileList += "$($file.Name) "
            } else {
                Write-ToScreen "File $filePath was not found. Ensure the file in the settings XML exists in the module" -Type Error
            }
        }
    }

    <#if ($fileType.ToLower() -eq "css" -or $fileType.ToLower() -eq "js") {
        
        Minify-Assets -files $fileList -type $fileType.ToLower()
        
        switch($fileType.ToLower()) {

            "css" {
                $filePath = ($Global:settings.Path.TrimEnd("\") + "\Dist\Css\" + $Global:settings.BrandingXml.configuration.branding.css.minified)
                Write-ToScreen -Message "Uploading $($Global:settings.BrandingXml.configuration.branding.css.minified)" -Type Ok
                Set-SPOFileCheckedOut -Url ($Global:settings.BrandingXml.configuration.branding.css.path.TrimEnd("/") + "/" + $Global:settings.BrandingXml.configuration.branding.css.minified) -ErrorAction SilentlyContinue       
                Upload-File -filePath $filePath -folder $Global:settings.BrandingXml.configuration.branding.css.path
            }

            "js" {
                $filePath = ($Global:settings.Path.TrimEnd("\") + "\Dist\js\" + $Global:settings.BrandingXml.configuration.branding.js.minified)
                Write-ToScreen -Message "Uploading $($Global:settings.BrandingXml.configuration.branding.js.minified)" -Type Ok
                Set-SPOFileCheckedOut -Url ($Global:settings.BrandingXml.configuration.branding.js.path.TrimEnd("/") + "/" + $Global:settings.BrandingXml.configuration.branding.js.minified) -ErrorAction SilentlyContinue       
                Upload-File -filePath $filePath -folder $Global:settings.BrandingXml.configuration.branding.js.path
            }

        }
    }#>
    
}



function Upload-File {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the path to the file you wish to upload?')]
	    [string]$filePath,
        
        [Parameter(Mandatory=$false, Position=2, ValueFromPipeline=$false, HelpMessage='Provide a relative URL to the you wish to upload to.')]
	    [string]$folder
    )

    $uploadFile = Add-SPOFile -Path $filePath -Folder $folder -Checkout
    CheckIn-File -file $uploadFile
}

function Upload-MasterPages {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the branding set you would like.')]
	    [string]$webApplication
    )
    
    foreach ($f in $Global:settings.BrandingXml.configuration.branding.$webApplication.masterpages.files.masterpage) {
        $filePath = ($Global:settings.Path.TrimEnd("\") + "\Dist\MasterPages\" + $f.name.TrimStart("\"))
        if (Test-Path $filePath) {
            Write-ToScreen -Message "Uploading $($f.name) master page to '$((Get-SPOContext).Url)/_catalogs/masterpage/$($f.folder)'" -Type Ok
            Upload-MasterPage -filePath $filePath -folder $f.folder -title $f.Title -description $f.description
        }   
    } 
    
    #$masterPageUrl = ((Get-SPOWeb).ServerRelativeUrl  + "/_catalogs/masterpage/InsideWellmark.master")
    #Set-SPOMasterPage -MasterPageServerRelativeUrl $masterPageUrl -CustomMasterPageServerRelativeUrl $masterPageUrl
}

function Upload-MasterPage {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the path to the file you wish to upload?')]
	    [string]$filePath,
        
        [Parameter(Mandatory=$false, Position=2, ValueFromPipeline=$false, HelpMessage='Provide a relative URL to the you wish to upload to.')]
	    [string]$folder,

        [Parameter(Mandatory=$false, Position=3, ValueFromPipeline=$false, HelpMessage='Provide a title.')]
	    [string]$title,

        [Parameter(Mandatory=$false, Position=4, ValueFromPipeline=$false, HelpMessage='Provide a description.')]
	    [string]$description
    )

    $uploadMaster = Add-SPOMasterPage -SourceFilePath $filePath -Title $title -Description $description -DestinationFolderHierarchy $folder -UiVersion 15
}

function Upload-PageLayouts {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$false, HelpMessage='Provide the branding set you would like.')]
	    [string]$webApplication
    )

    foreach ($f in $Global:settings.BrandingXml.configuration.branding.$webApplication.pagelayouts.files.pagelayout) {
        $filePath = ($Global:settings.Path.TrimEnd("\") + "\Dist\PageLayouts\" + $f.name.TrimStart("\"))
        if (Test-Path $filePath) {
            Write-ToScreen -Message "Uploading $($f.name) page layout to '$((Get-SPOContext).Url)/_catalogs/masterpage/$($f.folder)'" -Type Ok
            Upload-PageLayout -filePath $filePath -title $f.title -description $f.description -associatedContentTypeId $f.publishingAssociatedContentType -folder $f.folder
        }   
    }
}

function Upload-PageLayout {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide a path for the file.')]
	    [string]$filePath,

        [Parameter(Mandatory=$false, Position=2, ValueFromPipeline=$false, HelpMessage='Provide a title.')]
	    [string]$title,

        [Parameter(Mandatory=$false, Position=3, ValueFromPipeline=$false, HelpMessage='Provide a description.')]
	    [string]$description,
    
        [Parameter(Mandatory=$true, Position=4, ValueFromPipeline=$false, HelpMessage='Provide a the content type ID for this page layout.')]
	    [string]$associatedContentTypeId,

        [Parameter(Mandatory=$false, Position=5, ValueFromPipeline=$false, HelpMessage='Provide a folder path')]
	    [string]$folder
    )

    $uploadPubLayout = Add-SPOPublishingPageLayout -SourceFilePath $filePath -Title $title -Description $description -AssociatedContentTypeID $associatedContentTypeId -DestinationFolderHierarchy $folder
    #CheckIn-File -file $uploadPubLayout

}

function CheckIn-File {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide  a file context for the file you wish to check in.')]
	    $file
    )
    
    Set-SPOFileCheckedIn -Url $file.ServerRelativeUrl -CheckinType MajorCheckIn -Comment "Updated by ByronDeploy PNP PowerShell Module."
}

function CheckOut-File {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$false, Position=1, ValueFromPipeline=$false, HelpMessage='Provide  a file context for the file you wish to check in.')]
	    $serverRelativeUrl
    )
    
    Set-SPOFileCheckedOut -Url $serverRelativeUrl
}

function Minify-Assets {
    [CmdletBinding()]    
    param (
        [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$false, HelpMessage='Provide a string of space delimited files to minify.')]
	    [string]$files,

        [Parameter(Mandatory=$true, Position=3, ValueFromPipeline=$false, HelpMessage='Provide the to save the minified output.')]
	    [string]$type,
        
        [Parameter(Mandatory=$false, Position=4, ValueFromPipeline=$false, HelpMessage='Analyze the css or JS before compiling')]
	    [switch]$analyze
    )

    Write-ToScreen -Message "Minifiying $type files." -Type Ok

    if ($type.ToLower() -eq "css") { 
        $out = $Global:settings.BrandingXml.configuration.branding.css.minified
        $workingDirectory = $Global:settings.Path.TrimEnd("\") + "\Dist\css"
    } else { 
        $out = $Global:settings.BrandingXml.configuration.branding.js.minified
        $workingDirectory = $Global:settings.Path.TrimEnd("\") + "\Dist\js"
    }

    foreach ($f in $files.split(" ")) {
        $originalSize += $f.Length
    }

    Start-Process "$($Global:settings.Minifier)\ajaxMinifier.exe" -WorkingDirectory $workingDirectory -ArgumentList "$files -out $out"

}

#Push-ByronAssets -Site https://askbyron.sharepoint.com/sites/development -DeployBranding -Handlers css